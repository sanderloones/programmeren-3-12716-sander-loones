﻿using System.Web.Mvc;

namespace Webwinkel.Controllers
{
    public class CountryController : Controller
    {
        public ActionResult Editing()
        {
            
            return View();
        }

        public ActionResult Inserting()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Insert(string CountryCode, string CountryName,
       string CountryLanguage)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Code = CountryCode;
            country.Name = CountryName;
            country.Language = CountryLanguage;
            dal.DbSetCountry.Add(country);
            dal.SaveChanges();
            return View("Inserting");
        }
        public ActionResult ReadingOne(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = id;
            country = dal.DbSetCountry.Find(id);
            return View(country);
        }
        public ActionResult InsertingCancel()
        {
            
            return View("Editing");
        }
        public ActionResult Cancel()
        {
            return View("Editing");
        }
        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return PartialView(dal.DbSetCountry);
        }
        public ActionResult Delete(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country() { Id = id };
            dal.DbSetCountry.Attach(country);
            dal.DbSetCountry.Remove(country);
            dal.SaveChanges();
            return View("Editing");
        }
        public ActionResult Updating(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = id;
            country = dal.DbSetCountry.Find(id);
            return View(country);
        }

        [HttpPost]
        public ActionResult Update(string CountryCode, string CountryName,
        string CountryLanguage, string CountryId)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = System.Int32.Parse(CountryId);
            country.Code = CountryCode;
            country.Name = CountryName;
            country.Language = CountryLanguage;
            if (TryValidateModel(country))
            {
                dal.DbSetCountry.Attach(country);
                dal.Entry(country).State = System.Data.Entity.EntityState.Modified;
                dal.SaveChanges();
            }
            return View("ReadingOne", country);
        }

    }
}