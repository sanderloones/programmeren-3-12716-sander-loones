﻿using System.Web.Mvc;
using System;

namespace Webwinkel.Controllers
{
    public class OrderController : Controller
    {
        public ActionResult Editing()
        {
            
            return View();
        }

        public ActionResult Inserting()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Insert(string OrderIdStatus, DateTime OrderDate,
       DateTime ShippingDate, string idCustomer, string idShippingMethod )
        {
            Models.Dal dal = new Models.Dal();
            Models.Order order = new Models.Order();
            order.IdStatus = Convert.ToInt32(OrderIdStatus);
            order.OrderDate = OrderDate;
            order.ShippingDate = ShippingDate ;
            order.IdCustomer = Convert.ToInt32(idCustomer);
            order.IdShippingMethod = Convert.ToInt32(idShippingMethod);
            dal.DbSetOrder.Add(order);
            dal.SaveChanges();
            return View("Inserting");
        }
        public ActionResult ReadingOne(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Order order = new Models.Order();
            order.Id = id;
            order = dal.DbSetOrder.Find(id);
            return View(order);
        }
        public ActionResult InsertingCancel()
        {
            
            return View("Editing");
        }
        public ActionResult Cancel()
        {
            return View("Editing");
        }
        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return PartialView(dal.DbSetOrder);
        }
        public ActionResult Delete(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Order order = new Models.Order() { Id = id };
            dal.DbSetOrder.Attach(order);
            dal.DbSetOrder.Remove(order);
            dal.SaveChanges();
            return View("Editing");
        }
        public ActionResult Updating(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Order order = new Models.Order();
            order.Id = id;
            order = dal.DbSetOrder.Find(id);
            return View(order);
        }

        [HttpPost]
        public ActionResult Update(string OrderIdStatus, DateTime OrderDate,
       DateTime ShippingDate, string idCustomer, string idShippingMethod, string OrderId)
        {
            Models.Dal dal = new Models.Dal();
            Models.Order order = new Models.Order();
            order.Id = System.Int32.Parse(OrderId);
            order.IdStatus = Convert.ToInt32(OrderIdStatus);
            order.OrderDate = OrderDate;
            order.ShippingDate = ShippingDate;
            order.IdCustomer = Convert.ToInt32(idCustomer);
            order.IdShippingMethod = Convert.ToInt32(idShippingMethod);
            if (TryValidateModel(order))
            {
                dal.DbSetOrder.Attach(order);
                dal.Entry(order).State = System.Data.Entity.EntityState.Modified;
                dal.SaveChanges();
            }
            return View("ReadingOne", order);
        }

    }
}