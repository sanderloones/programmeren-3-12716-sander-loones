﻿using System.Web.Mvc;
using System.Collections.Generic;
using System;

namespace Webwinkel.Controllers
{
    public class OrderStatusController : Controller
    {
        public ActionResult Editing()
        {
            
            return View();
        }

        public ActionResult Inserting()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Insert(string OrderStatusName,
       string OrderStatusDescription)
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            model.Name = OrderStatusName;
            model.Description = OrderStatusDescription;
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            dal.Create();
            //Models.Dal dal = new Models.Dal();
            //Models.OrderStatus orderStatus = new Models.OrderStatus();
            //orderStatus.Name = OrderStatusName;
            //orderStatus.Description = OrderStatusDescription;
            //dal.DbSetOrderStatus.Add(orderStatus);
            //dal.SaveChanges();
            return View("Inserting");
        }
        public ActionResult ReadingOne(int id)
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            model.Id = id;
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            dal.ReadOne();
            
            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Id = dal.Model.Id;
            orderStatus.Name = dal.Model.Name;
            orderStatus.Description = dal.Model.Description;
            //orderStatus = dal.DbSetOrderStatus.Find(id);
            return View(orderStatus);
        }
        public ActionResult InsertingCancel()
        {
            
            return View("Editing");
        }
        public ActionResult Cancel()
        {
            return View("Editing");
        }
        public ActionResult ReadingAll()
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            List<Webwinkel.Models.OrderStatus> list = dal.ReadAll();
            //Models.Dal dal = new Models.Dal();
            return PartialView(list);
        }
        public ActionResult Delete(int id)
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            model.Id = id;
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            dal.Delete();
            return View("Editing");
        }
        public ActionResult Updating(int id)
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            model.Id = id;
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            dal.ReadOne();

            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Id = dal.Model.Id;
            orderStatus.Name = dal.Model.Name;
            orderStatus.Description = dal.Model.Description;
            //Models.Dal dal = new Models.Dal();
            //Models.OrderStatus orderStatus = new Models.OrderStatus();
            //orderStatus.Id = id;
            //orderStatus = dal.DbSetOrderStatus.Find(id);
            return View(orderStatus);
        }

        [HttpPost]
        public ActionResult Update( string OrderStatusName,
        string OrderStatusDescription, string OrderStatusId)
        {
            Webwinkel.Models.OrderStatus model = new Webwinkel.Models.OrderStatus();
            model.Id = Int32.Parse(OrderStatusId);
            model.Name = OrderStatusName;
            model.Description = OrderStatusDescription;
            Webwinkel.Dal.OrderStatus dal = new Webwinkel.Dal.OrderStatus(model);
            dal.Update();

            Models.OrderStatus orderStatus = model;
            
            //Models.Dal dal = new Models.Dal();
            //Models.OrderStatus orderStatus = new Models.OrderStatus();
            //orderStatus.Id = System.Int32.Parse(OrderStatusId);
            //orderStatus.Name = OrderStatusName;
            //orderStatus.Description = OrderStatusDescription;
            //dal.DbSetOrderStatus.Add(orderStatus);
            //dal.SaveChanges();
            //if (TryValidateModel(orderStatus))
            //{
            //    dal.DbSetOrderStatus.Attach(orderStatus);
            //    dal.Entry(orderStatus).State = System.Data.Entity.EntityState.Modified;
            //    dal.SaveChanges();
            //}
            return View("ReadingOne", orderStatus);
        }

    }
}