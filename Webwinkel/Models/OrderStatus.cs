﻿
using System;

namespace Webwinkel.Models
{
    public class OrderStatus
    {
        // fields
        protected String name;
        protected String description;
        protected Int32 id;
        // Getters and setters
        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public String Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

    }
}