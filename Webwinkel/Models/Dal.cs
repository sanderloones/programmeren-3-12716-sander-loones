﻿
using System.Data.Entity;

namespace Webwinkel.Models
{
    public class Dal : DbContext
    {
        public Dal() : base("name=WebwinkelWindowsAuthentication")
        { }
        public virtual DbSet<UnitBase> DbSetUnitBase { get; set; }
        public virtual DbSet<Country> DbSetCountry { get; set; }
        public virtual DbSet<OrderStatus> DbSetOrderStatus { get; set; }
        public virtual DbSet<Order> DbSetOrder { get; set; }
    }

}