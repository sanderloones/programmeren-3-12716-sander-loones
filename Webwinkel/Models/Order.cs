﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Webwinkel.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "datetime")]
        [DisplayName("Besteldatum")]
        public DateTime OrderDate { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "datetime")]
        [DisplayName("Verzenddatum")]
        public DateTime ShippingDate { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Column(TypeName = "int")]
        [DisplayName("Klant")]
        public int IdCustomer { get; set; }

        [Column(TypeName = "int")]
        [DisplayName("Verzendigsmethode")]
        public int IdShippingMethod { get; set; }

        [Column(TypeName = "int")]
        [DisplayName("Status")]
        public int IdStatus { get; set; }


    }
}