
CREATE DATABASE Bibliotex
ON
-- Because neither MB nor KB is specified in the SIZE parameter for the Sales_dat file, 
-- it uses MB and is allocated in megabytes.
( NAME = Bibliotex,
    FILENAME = 'D:\Programmeren3\Bibliotex01\App_Data\Bibliotex.mdf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 )
LOG ON
(NAME = Bibliotex_log,
    FILENAME = 'D:\Programmeren3\Bibliotex01\App_Data\Bibliotex_log.ldf',
    SIZE = 5MB,
    MAXSIZE = 25MB,
    FILEGROWTH = 5MB );
GO

