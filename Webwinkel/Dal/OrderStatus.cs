﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
namespace Webwinkel.Dal
{
    class OrderStatus : Dal.IDal<Models.OrderStatus>
    {
        protected string message;
        public string Message
        {
            get { return this.message; }
        }
        protected Models.OrderStatus model;
        public Models.OrderStatus Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        public OrderStatus(Models.OrderStatus model)
        {
            this.model = model;
        }

        public List<Models.OrderStatus> ReadAll()
        {
            List<Models.OrderStatus> list = new List<Models.OrderStatus>();
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["WebwinkelWindowsAuthentication"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen we de naam
            // van de stored procedure
            string sqlString = "OrderStatusSelectAll";
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";

                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " OrderStatus rijen gevonden.";
                            while (result.Read())
                            {
                                Webwinkel.Models.OrderStatus orderStatus =
                                    new Models.OrderStatus();
                                orderStatus.Name =
                                    (result.IsDBNull(result.GetOrdinal("Name")) ? "" :
                                    result[result.GetOrdinal("Name")].ToString());
                                orderStatus.Id =
                                    (result.IsDBNull(result.GetOrdinal("Id")) ? 0 :
                                    Int32.Parse(result[result.GetOrdinal("Id")].ToString()));
                                list.Add(orderStatus);
                            }
                        }
                        else
                        {
                            this.message = " OrderStatus rijen niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return list;
        }


        public int Create()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["WebwinkelWindowsAuthentication"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen de naam
            // van de stored procedure
            string sqlString = "OrderStatusInsert";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar, 255)).Value = Model.Name;
            command.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar, 1024)).Value = Model.Description;
            SqlParameter id = new SqlParameter("@Id", SqlDbType.Int);
            id.Direction = ParameterDirection.Output;
            command.Parameters.Add(id);
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            int result = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    result = command.ExecuteNonQuery();
                    // we moeten kijken naar de waarde van out parameter
                    // van Insert stored procedure. Als de insert niet lukt
                    // retourneert de out parameter van de stored procedure 0
                    if (result <= 0)
                    {
                        this.message = " OrderStatus is niet geïnserted.";
                    }
                    else
                    {
                        this.message = " OrderStatus is geïnserted.";
                        result = (int)id.Value;
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return result; // 0 of de Id van de nieuwe rij
        }

        public Models.OrderStatus ReadOne()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["WebwinkelWindowsAuthentication"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen we de naam
            // van de stored procedure
            string sqlString = "OrderStatusSelectOne";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int)).Value = Model.Id;
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            SqlDataReader result = null;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    this.message = "Connectie is open";
                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            // lees de gevonden rij in
                            this.message = " OrderStatus is gevonden.";
                            result.Read();
                            Model.Name =
                                (result.IsDBNull(result.GetOrdinal("Name")) ? "" :
                                result.GetString(result.GetOrdinal("Name")));
                            Model.Description =
                                (result.IsDBNull(result.GetOrdinal("Description")) ? "" :
                                result.GetString(result.GetOrdinal("Description")));
                            Model.Id =
                                (result.IsDBNull(result.GetOrdinal("Id")) ? 0 :
                                result.GetInt32(result.GetOrdinal("Id")));
                        }
                        else
                        {
                            this.message = " OrderStatus rij niet gevonden.";
                        }
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return Model;
        }

        public int Update()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["WebwinkelWindowsAuthentication"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen de naam
            // van de stored procedure
            string sqlString = "OrderStatusUpdate";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar, 255)).Value = Model.Name;
            command.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar, 1024)).Value = Model.Description;
            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Model.Id;
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            int result = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    result = command.ExecuteNonQuery();
                    // we moeten kijken naar de return van ExecuteNonQuery
                    // retourneert het aantal rijen dat gedeleted is
                    if (result == 0)
                    {
                        this.message = " OrderStatus is niet geüpdated.";
                    }
                    else
                    {
                        this.message = " OrderStatus is geüpdated.";
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return result; // 0 of de Id van de nieuwe rij
        }

        public int Delete()
        {
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.
                ConnectionStrings["WebwinkelWindowsAuthentication"].ToString();
            SqlCommand command = new SqlCommand();
            // in de CommandText eigenschap stoppen de naam
            // van de stored procedure
            string sqlString = "OrderStatusDelete";
            //we use here a getter method to obtain the value to be saved,
            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = Model.Id;
            // zeg aan het command object dat het een stored procedure
            // zal krijgen en geen SQL Statement
            command.CommandType = CommandType.StoredProcedure;
            // stop het sql statement in het command object
            command.CommandText = sqlString;
            // geeft het connection object door aan het command object
            command.Connection = connection;
            this.message = "Niets te melden";
            // we gaan ervan uit dat het mislukt
            int result = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    //Verbinding geslaagd
                    result = command.ExecuteNonQuery();
                    // we moeten kijken naar de return van ExecuteNonQuery
                    // retourneert het aantal rijen dat gedeleted is
                    if (result == 0)
                    {
                        this.message = " OrderStatus is niet gedeleted.";
                    }
                    else
                    {
                        this.message = " OrderStatus is gedeleted.";
                    }
                }
                catch (SqlException e)
                {
                    this.message = e.Message;
                }
            }
            return result;
        }

    }
}