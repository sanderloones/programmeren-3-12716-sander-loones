﻿using System.Collections.Generic;

namespace Webwinkel.Dal
{
    interface IDal<T>
    {
        string Message { get; }
        int Create();
        int Update();
        int Delete();
        T ReadOne();
        List<T> ReadAll();
    }
}